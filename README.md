# Petal Hero

Rhythm game for the [flow3r badge](https://flow3r.garden/). 

![Petal Hero](https://private-user-images.githubusercontent.com/126204/286000622-7de711cb-af7a-43a7-8f6e-f1733676f4ec.jpg?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MDU2OTc1MjMsIm5iZiI6MTcwNTY5NzIyMywicGF0aCI6Ii8xMjYyMDQvMjg2MDAwNjIyLTdkZTcxMWNiLWFmN2EtNDNhNy04ZjZlLWYxNzMzNjc2ZjRlYy5qcGc_WC1BbXotQWxnb3JpdGhtPUFXUzQtSE1BQy1TSEEyNTYmWC1BbXotQ3JlZGVudGlhbD1BS0lBVkNPRFlMU0E1M1BRSzRaQSUyRjIwMjQwMTE5JTJGdXMtZWFzdC0xJTJGczMlMkZhd3M0X3JlcXVlc3QmWC1BbXotRGF0ZT0yMDI0MDExOVQyMDQ3MDNaJlgtQW16LUV4cGlyZXM9MzAwJlgtQW16LVNpZ25hdHVyZT0xZTFhOWVmZmEyM2E5M2M4MmU2NTJkZjkzNGQ3OTQyNTZlMDM2NzcwNDY1Y2JiMDEzNDk2MTA1ZmI4YTg2MjAzJlgtQW16LVNpZ25lZEhlYWRlcnM9aG9zdCZhY3Rvcl9pZD0wJmtleV9pZD0wJnJlcG9faWQ9MCJ9.lPCF1gHwrfb74KEWjvYh0vwshxAxU81Ifjktzx4v8R8)

See [the gameplay video](https://social.librem.one/@dos/111478238181935805).

## Songs

*Petal Hero* is compatible with songs for Frets on Fire, FoFiX, Performous,
Phase Shift and Clone Hero (MIDI) that contain a guitar track, but with one
caveat: you need to mix audio tracks together and save them as MP3.

This should do:

```sh
sox *.ogg -m -G -c 1 -C 128 -r 48k --norm=-3 song.mp3
```

Some rips may need to be resampled first:

```sh
for i in *.ogg; do sox $i -G -r 48k $i.flac; done
sox *.flac -m -G -c 1 -C 128 -r 4/8k --norm=-3 song.mp3
```

You need *song.ini*, *song.mp3* and *notes.mid* in the song directory.

Songs in *.chart* format (and some others) can be converted using [EOF](https://github.com/raynebc/editor-on-fire).

The "Starter Pack" of songs can be downloaded over Wi-Fi from the game, or manually
from [the song repository](https://git.flow3r.garden/dos/PetalHero-songs/).

Some places to find charted songs at:
 - https://chorus.fightthe.pw/
 - https://db.c3universe.com/songs/
 - https://sourceforge.net/p/fretsonfire/code/HEAD/tarball?path=/trunk/data/songs

## License

*Petal Hero* is licensed under the GNU General Public License version 3 or later.

Sound assets and parts of the code come from *Frets on Fire* by Sami Kyöstilä,
Tommi Inkilä, Joonas Kerttula and Max M., originally licensed under the GNU
General Public License version 2 or later.

Created by [Sebastian Krzyszkowiak](https://dosowisko.net).
